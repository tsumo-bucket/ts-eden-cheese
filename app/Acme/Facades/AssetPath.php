<?php

namespace Acme\Facades;

use Illuminate\Support\Facades\Facade;

class AssetPath extends Facade 
{
	protected static function getFacadeAccessor()
	{
		return 'assetpath';
	}
}