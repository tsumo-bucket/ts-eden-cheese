<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VideoBannerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'frametag' => 'required',
            'type' => 'required|max:255',
            'option_id' => 'integer',
            'video_id' => 'integer',
        ];
    }
}
