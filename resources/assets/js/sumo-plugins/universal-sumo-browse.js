 /*
  * Universal Selector (uni-selector)
  *
  * Developed by Biboy Oyales
  */

(function ($) {
    $.fn.uniSelector = function (options) {
        var xhrRequest = false,
            selectorItemTemplate = Handlebars.compile($("#defaultItemTemplate").html()),
            selectorItemVariantTemplate = Handlebars.compile($("#defaultVariantTemplate").html()),
            onConfirmCallBack;
        
        if (typeof _.each === 'undefined') {
            throw new Error('sumo-browse requires Underscore.js to be loaded first');
        }
        
        if (typeof Handlebars === 'undefined') {
            throw new Error('sumo-browse requires Handlebars.js to be loaded first');
        }

        var defaults = {
            multiple: true, // bool | multiple select for values, returns array, if false, returns object,
            title: 'Browse', // string | header title
            confirmButtonText: 'Select', // string | default/override confirm button text 
            cancelButtonText: 'Cancel', // string | default/override cancel button text
            data: [], // array | default/override array of objects. fields required: { values |object|, key |string|integer|, text |string|, html |string|, children |array| } 
            disableSelected: false, // bool | default/override disabling selected checkboxes 
			columnHeader: '', // string | custom column header
            typingInterval: 500, // number | default typing interval for each key down,
            filters: [], //--- list of data filters,
            imgPlaceholder: 'fal fa-box',
            page: {
                size: 20, //--- number of rows to fetch per request **note that you can pass 0 to this property to indicate a get all request
                current: 0 //--- current number request(s)
            },
            itemsQuantifiable: false, // boolean flag if items are quantifiable or not
            maxQuantity: 0, // max quantity for every item that is quantifiable
            displayTotal: {}, // object or array of objects to be added up and display in the selector
            orderSelector: false, // boolean flag for initializing a selector for order making,
            withDiscountForm: false // add a input text for discount value
        };

        /**
         * displayTotal : {
         *      label: string //--- label to be displayed
         *      objectKey: string //--- object key to look for
         *      options: {
         *          decimal: integer // ---number of decimal places
         *          combineWith: string //--- object key to be combined with the objectKey prop
         *          combineOperation: char //--- mathematical operation to be used for the combination
         *      }
         * }
         */

        /*!CHEAT SHEET FOR FILTER FORMAT
            ** filter format:
            [
                {
                    attr_name: attr_value
                }
            ]
            ** filter sample format:
            [
                {
                    name: "first_name",
                    type: "text", //@value text|date|select
                    options: {
                        value: "text" //@options attribute is only applicable for select types only
                    }
                }
            ]
        */
        
        // Extend those options
		options = $.extend(defaults, options);
        
        var _selected = [], //--- confirmed selected
            _currentSelected = [], //--- unconfirmed selected
            _tableData, //--- data fetched from the api endpoint
            requestFilters = initializeFilters(options.filters), //-- request filters
            typingTimer, //--- interval between api requests while typing
            contentTemplate, //--- tempalte for the content data
            elID = sumoGUID(), //--- unique ID for the selector
            uniSelectorTemplate = Handlebars.compile($("#uniSelectoremplate").html()), //--- tempalte for the whole modal
            defaultSelectedItemTemplate = Handlebars.compile($("#defaultSelectedItemTemplate").html()), //-- template for the selected items
            pageStatus = { //-- pagination status
                'loaded': 0, //-- current rows loaded
                'all': 0 //-- overall rows count from the current query/filter
            };
        
        /* Initialize templates */
        $('#sumo-browse-modals')
            .append(
                uniSelectorTemplate({
                    id: elID,
                    multiple: options.multiple,
                    filters: options.filters,
                    displayTotal: filterDataTotals()
                })
            );
        var modal = $('#uniSelector-' + elID);

        $(options.selectedDataContainer)
            .html(`
                <div class="empty-message">
                    ${options.emptyMessage ? options.emptyMessage : 'No items selected'}
                </div>
            `);

        modal.on('show.bs.modal', function() {
            _currentSelected = _selected.slice();
        });

        modal.on('shown.bs.modal', function() {
            modal.find('.modal-header-title').html(options.title);
            modal.find('.modal-confirm-button').html(options.confirmButtonText);
            modal.find('.modal-cancel-button').html(options.cancelButtonText);
            modal.find('.modal-column-header').html(options.columnHeader);
            modal.find('.selected-container-header h4').html('Selected ' + (options.dataCategory ? options.dataCategory : ''));
        });

        modal.on('hidden.bs.modal', function() {
            _currentSelected = [];
            options.page.current = 0;
            updateTableData([]);
            clearFilters();
        });
        
        if (options.multiple) {
            contentTemplate = Handlebars.compile($("#sumo-browse-content-multiple-template").html());
        } else {
            contentTemplate = Handlebars.compile($("#sumo-browse-content-single-template").html());
        }

        if (options.selectedDataContainer) {
            $(options.selectedDataContainer).addClass("uni-selector-selected-items");
        }

        //--- modal selection on check event
        $(document).on('change', '#uniSelector-' + elID + '.uni-selector form input[type="checkbox"], #uniSelector-' + elID + '.uni-selector form input[type="radio"]', function(e) {
            if (!e.target.checked) {
                if (e.target.name == 'select_all') {
                    _currentSelected = [];
                } else {
                    var itemObj = JSON.parse(e.target.value);
                    _currentSelected = _.filter(_currentSelected, function(item) {
                        return item.key != itemObj.key;
                    });
                }
            }

            updateCurrentlySelectedData();
            $(this).parents('.mdc-checkbox').toggleClass('disabled');
        });

        //--- selected items on remove button click
        $(document).on('click', '.remove-btn.uniselector-rmb-' + elID, function(e) {
            e.preventDefault();
            e.stopPropagation();
            var key = $(this).attr('key');
            
            if ($(this).parents('.uni-selector-selected-items:not(.single-item-selection)').length > 0) {
                $(this).parents('.selected-item').remove();
                
                _selected = _.filter(_selected, function(item) {
                    return item.key != key;
                });
                
                _currentSelected = _selected.slice();

                if (options.selectedDataContainer && _selected.length == 0) {
                    $(options.selectedDataContainer).html(`
                        <div class="empty-message">
                            ${options.emptyMessage ? options.emptyMessage : 'No items selected'}
                        </div>
                    `);
                }
            } else {
                _currentSelected = _.filter(_currentSelected, function(item) {
                    return item.key != key;
                });

                if (options.multiple) {
                    $(`#uniSelector-${elID}.uni-selector form input[type="checkbox"][key=${key}]`).click();
                } else {
                    $(`#uniSelector-${elID}.uni-selector form input[type="radio"][data-key=${key}]`).prop('checked',false);
                    updateCurrentlySelectedData();
                }
            }

            if (onConfirmCallBack) {
                onConfirmCallBack(_selected);
            }
        });

        //--- selected item on quantity change
        $(document)
            .on('input', '.qty-control-' + elID, function(e) {
                var ID = $(this).attr("id").replace('_qty' + elID, '');
                
                if (e.target.value < $(this).attr("min")) {
                    e.target.value = $(this).attr("min");
                }
                
                if ($(this).hasClass("selected-qty")) {
                    _.find(_selected, function(searchItem) {
                        if (searchItem.key == ID) {
                            searchItem.values.quantity = e.target.value;
                            return 1;
                        }
                    });
                    
                    if (options.orderSelector) {
                        processSelectedAddonsData(_selected);
                    }

                    displaySelectedData();
                    onConfirmCallBack(_selected);
                } else {

                    _.find(_currentSelected, function(searchItem) {
                        if (searchItem.key == ID) {
                            searchItem.values.quantity = e.target.value;
                            return 1;
                        }
                    });

                    if (options.orderSelector) {
                        processSelectedAddonsData(_currentSelected);
                    }
                    
                    updateCurrentlySelectedData();
                }

                displayDataTotals(processDataTotals());
            })
            //-- addon qty change
            .on('input', '.addon-qty-' + elID, function(e) {
                var group = $(this).attr("group"),
                    product = $(this).attr("product_key"),
                    option = $(this).attr("option");

                if (e.target.value < $(this).attr("min")) {
                    e.target.value = $(this).attr("min");
                }
                
                if ($(this).hasClass("selected-qty")) {

                    _.find(_selected, function(searchItem) {
                        if (searchItem.key == product) {
                            _.find(searchItem.selectedAddons, function(sAddon) {
                                if (sAddon.group == group) {
                                    
                                    if (option == undefined || option == 0) {
                                        sAddon.quantity = e.target.value;
                                    } else {
                                        _.find(sAddon.selectedOptions, function(sOption) {
                                            if (sOption.option == option) {
                                                sOption.quantity = e.target.value;
                                                return 1;
                                            }
                                        });
                                    }
    
                                    _.find(searchItem.values.add_ons, function(addon) {
                                        if (addon.id == group) {
                                            if (option == undefined || option == 0) {
                                                addon.selected.quantity = e.target.value;
                                            } else {
                                                _.find(addon.items, function(addonOption) {
                                                    if(addonOption.id == option) {
                                                        addonOption.selected.quantity = e.target.value;
                                                    }
                                                });
                                            }
                                        }
                                    });
    
                                    return 1;
                                }
                            });
                            
                            return 1;
                        }
                    });

                    processSelectedAddonsData(_selected);
                    displaySelectedData();
                    onConfirmCallBack(_selected);
                } else {

                    _.find(_currentSelected, function(searchItem) {
                        if (searchItem.key == product) {
                            _.find(searchItem.selectedAddons, function(sAddon) {
                                if (sAddon.group == group) {
                                    
                                    if (option == undefined || option == 0) {
                                        sAddon.quantity = e.target.value;
                                    } else {
                                        _.find(sAddon.selectedOptions, function(sOption) {
                                            if (sOption.option == option) {
                                                sOption.quantity = e.target.value;
                                                return 1;
                                            }
                                        });
                                    }
    
                                    _.find(searchItem.values.add_ons, function(addon) {
                                        if (addon.id == group) {
                                            if (option == undefined || option == 0) {
                                                addon.selected.quantity = e.target.value;
                                            } else {
                                                _.find(addon.items, function(addonOption) {
                                                    if(addonOption.id == option) {
                                                        addonOption.selected.quantity = e.target.value;
                                                    }
                                                });
                                            }
                                        }
                                    });
    
                                    return 1;
                                }
                            });
                            
                            return 1;
                        }
                    });

                    processSelectedAddonsData();
                    updateCurrentlySelectedData();
                }
            });
        
        updateTableData(options.data);
        displaySelectedData();

        //--- on target(trigger button) click
        $(this).on('click', function(e) {
            showModal(true);
        });
        
        function fetchData() {
            loader(true);

            try {
                xhrRequest.abort();
            } catch(e) {}
            
            xhrRequest = $.ajax({
                type: "GET",
                url: options.apiURL,
                data: {filters: requestFilters, page: options.page},
                dataType: "JSON",
                success: function (result) {
                    var _parsedData = [];

                    if (result.data.length > 0) {
                        _.map(result.data, function (val) {
                            
                            if (val.children.length > 0) {
                                _.each(val.children, function(child) {
                                    child.values.quantifiable = options.itemsQuantifiable;
                                    child.html = selectorItemVariantTemplate({item: child.values, options: options});
                                });
                            }

                            val.quantifiable = options.itemsQuantifiable;
                            
                            _parsedData.push({
                                children: val.children,
                                values: _.omit(val, 'variants', 'children'),
                                key: val.id,
                                html: selectorItemTemplate({item: val, options: options}),
                            });
                        });
                    }

                    if (result.page_status) {
                        pageStatus = result.page_status;
                    }

                    updateData(_parsedData, _currentSelected);
                    loader(false);
                },
                error: function (xhr) {
                    console.log(xhr);
                },
            });
        }
		        
        /* SHOW/HIDE BROWSE MODAL FUNCTION 
         * @params status = |bool|required| action parameter to show/hide modal
         */
        function showModal(status) {
            var status = (status) ? 'show' : 'hide';
            modal.modal(status);
            initSelectedDataToTable(_tableData);
            fetchData();
        }

        function initSelectedDataToTable(data) {
            var selectedParent = [];
            if (options.multiple) {
                _.map(data, function (val, key) {
                    var parentDepth = modal.find('.depth-parent[key="' + val.key + '"]');
                    var parentMDC = parentDepth.find('.mdc-checkbox');
                    var parentCheckbox = parentDepth.find('[type="checkbox"]');
                    var parentMDCCheckbox = new MDCCheckbox(parentMDC[0]);
                    
                    parentCheckbox
                        .attr({
                            'disabled': false,
                            'key': val.key
                        });

                    parentMDC.toggleClass('disabled', false);

                    if (!_.isUndefined(val.children) && val.children.length > 0) {
                        var selectedChildren = [];
                        _.map(val.children, function (child, key) {
                            var isSelected = _.findWhere(_currentSelected, { key: child.key }),
                                childDepth = modal.find('.depth-child[key="' + child.key + '"]'),
                                childMDC = childDepth.find('.mdc-checkbox'),
                                childCheckbox = childDepth.find('[type="checkbox"]');

                            childCheckbox.attr({
                                'disabled': false,
                                'key': child.key
                            });

                            childMDC.toggleClass('disabled', false);
                            if (!_.isUndefined(isSelected)) {
                                selectedChildren.push(isSelected);
                                childCheckbox.prop('checked', true);
                                childCheckbox
                                if (options.disableSelected) {
                                    // childCheckbox.attr('disabled', true);
                                    childMDC.toggleClass('disabled', true);
                                }
                            } else {
                                childCheckbox.prop('checked', false);
                            }
                        });

                        if (selectedChildren.length == val.children.length) {
                            selectedParent.push(val);
                            parentCheckbox.prop('checked', true);
                            parentMDCCheckbox.checked = true;
                            parentMDCCheckbox.indeterminate = false;
                            if (options.disableSelected) {
                                // parentCheckbox.attr('disabled', true);
                                parentMDC.addClass('disabled');
                            }
                        } else {
                            parentCheckbox.prop('checked', false);
                            if (selectedChildren.length > 0) {
                                parentMDCCheckbox.indeterminate = true;
                                parentMDCCheckbox.checked = false;
                            } else {
                                parentMDCCheckbox.checked = false;
                                parentMDCCheckbox.indeterminate = false;
                            }
                        }
                    } else {
                        var isSelected = _.findWhere(_currentSelected, { key: val.key });
                        if (!_.isUndefined(isSelected)) {
                            selectedParent.push(isSelected);
                            parentCheckbox.prop('checked', true);
                            if (options.disableSelected) {
                                // parentCheckbox.attr('disabled', true);
                                parentMDC.addClass('disabled');
                            }
                        } else {
                            parentCheckbox.prop('checked', false);
                        }
                    }
                });

                var select_all_checkbox = modal.find('[name="select_all"]'),
                    select_all_MDC = select_all_checkbox.closest('.mdc-checkbox'),
                    select_all_MDCCheckbox = new MDCCheckbox(select_all_MDC[0]);

                if (selectedParent.length == data.length) {
                    select_all_checkbox.prop('checked', true);
                    select_all_MDCCheckbox.checked = true;
                    select_all_MDCCheckbox.indeterminate = false;
                } else {
                    select_all_checkbox.prop('checked', false);
                    if (selectedParent.length > 0) {
                        select_all_MDCCheckbox.indeterminate = true;
                        select_all_MDCCheckbox.checked = false;
                    } else {
                        select_all_MDCCheckbox.checked = false;
                        select_all_MDCCheckbox.indeterminate = false;
                    }
                }
            } else {
                if (_currentSelected.length > 0) {
                    modal.find('[data-key="' + _currentSelected[0].key + '"]').prop("checked", true);
                }
            }
        }

        /* UPDATE TABLE DATA FUNCTION 
         * @params data = |array|required| fields required: { values, key, text, html, children }
         */
        function updateTableData(data) {
            _tableData = data;
            
            _.map(data, function (val, key) {
                val.parentDepth = 0;
                
                if (!_.isUndefined(val.children)) {
                    if (val.children.length > 0) {
                        val.parentDepth = 1;
                        _.map(val.children, function (child, key) {
                            child.parent = val.key;
                            child.serialized = JSON.stringify({
                                key: child.key,
                                name: child.name,
                                values: child.values,
                                parent: child.parent
                            });
                        });
                    }
                }

                val.serialized = JSON.stringify({
                    key: val.key,
                    name: val.values.name,
                    values: val.values,
                    parentDepth: val.parentDepth,
                    children: val.children != undefined ? val.children : []
                });
            });

            modal.find('.modal-body-content tbody').html(contentTemplate(data));
            initSelectedDataToTable(data);
            updateCurrentlySelectedData();
        }

        function updateCurrentlySelectedData() {
            var form = modal.find('form'),
                        serialized = form.serializeArray(),
                        tmpSelected = [],
                        _currentSelectedCopy = _currentSelected.slice(),
                        selectedCount = 0;
            
            if (_tableData.length > 0) {
                if (options.multiple) {
                    _currentSelected = [];
                    //--- filter out all selected items that is not in the data fetched
                    _.each(_currentSelectedCopy, function(currentCopy) {
                        var searchPool = _tableData;
    
                        if (currentCopy.parent != undefined) {
                            if (currentCopy.parent > 0) {
                                var parent = _.find(_tableData, function(parentData) {
                                    return parentData.key == currentCopy.parent;
                                });
                                
                                if (parent) {
                                    searchPool = parent.children;
                                }
                            }
                        }
    
                        if (!_.find(searchPool, function(data) {
                            return data.key == currentCopy.key;
                        })) {
                            currentCopy.animationDelay = '0s';
                            currentCopy.animationDuration = '0s';
                            if (currentCopy.timestamp == undefined) {
                                currentCopy.timestamp = moment().unix() + selectedCount;
                            }
                            tmpSelected.push(currentCopy);
                            selectedCount++;
                        }
                    });

                    _.each(serialized, function (val) {
                        if (val.name == 'checkboxes[]') {
                            try {
                                var itemSearch;
                                val = JSON.parse(val.value);
                                if (val.parent == undefined && val.children.length == 0) {
                                    itemSearch = _.find(_tableData, function(data) { 
                                        if (val.key == data.key) {
                                            data.animationDelay = '0s';
                                            data.animationDuration = '0s';
                                            data.timestamp = moment().unix() + selectedCount;
    
                                            if (!_.find(_currentSelectedCopy, function(selected) {
                                                if (selected.key == data.key) {
                                                    data.timestamp = selected.timestamp;
                                                    data.values.quantity = selected.values.quantity;

                                                    if (options.orderSelector) {
                                                        data.selectedAddons = selected.selectedAddons.slice();

                                                        _.each(data.selectedAddons, function(sAddon) {
                                                            _.find(data.values.add_ons, function(addOn) {
                                                                if (addOn.id == sAddon.group) {
                                                                    addOn.selected = sAddon;

                                                                    if (sAddon.type == 'option') {
                                                                        _.each(sAddon.selectedOptions, function(sOption) {
                                                                            _.find(addOn.items, function(option) {
                                                                                if(option.id == sOption.option) {
                                                                                    option.selected = sOption;
                                                                                    return 1;
                                                                                }
                                                                            });
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }

                                                    return 1;
                                                }
                                            })) {
                                                data.animationDuration = '0.25s';
                                                data.animationDelay = selectedCount * 0.1 + 's';
                                                selectedCount++;
                                            }
    
                                            if (!_.find(tmpSelected, function(item) {
                                                return item.key == data.key;
                                            })) {
                                                return true;
                                            }
    
                                            return false; 
                                        }
                                    });
                                } else {
                                    var parent = _.find(_tableData, function(parentData) {
                                        return parentData.key == val.parent;
                                    });
                                    
                                    itemSearch = _.find(parent.children, function(data) { 
                                        if (val.key == data.key) {
                                            data.animationDelay = '0s';
                                            data.animationDuration = '0s';
                                            data.timestamp = moment().unix() + selectedCount;
    
                                            if (!_.find(_currentSelectedCopy, function(selected) {
                                                if (selected.key == data.key) {
                                                    data.timestamp = selected.timestamp;
                                                    data.values.quantity = selected.values.quantity;
                                                    return 1;
                                                }
                                            })) {
                                                data.animationDuration = '0.25s';
                                                data.animationDelay = selectedCount * 0.1 + 's';
                                                selectedCount++;
                                            }
    
                                            if (!_.find(tmpSelected, function(item) {
                                                return item.key == data.key;
                                            })) {
                                                return true;
                                            }
    
                                            return false; 
                                        }
                                    });
                                }
    
                                if (itemSearch) {
                                    tmpSelected.push(itemSearch);
                                }
                            } catch (e) { /* do some catching here :) */ }
                        }
                    });

                    tmpSelected = _.sortBy(tmpSelected, function(selected) { return selected.timestamp; });
                    _currentSelected = tmpSelected.slice();

                    //-- this process is specifically for order type selector
                    if (options.orderSelector) {
                        processSelectedAddonsData();
                    }
                } else {
                    if (serialized[0]) {
                        if (serialized[0].name == 'checkboxes[]') {
                            var itemSearch;
                            var value = JSON.parse(serialized[0].value);
                            var isNewData = false;

                            if (_currentSelected.length == 0) {
                                isNewData = true;
                            } else {
                                if (_currentSelected[0].key != value.key) {
                                    isNewData = true;
                                } else {
                                    _currentSelected[0].animationDuration = '0s';
                                }
                            }
        
                            if (isNewData) {
                                itemSearch = _.find(_tableData, function(data) { 
                                    if (value.key == data.key) {
                                        data.animationDelay = '0s';
                                        data.animationDuration = '0.25s';
                                        data.timestamp = moment().unix() + selectedCount;
                                        return true; 
                                    }
                                });
        
                                _currentSelected = [itemSearch];
                            }
                        }
                    }
                }
            }

            displayDataTotals(processDataTotals());
            modal.find('.selected-container .total-selected-counter span').html(_currentSelected.length);
            modal.find('.selected-container .selected-list').html(defaultSelectedItemTemplate({items: _currentSelected, uniID: elID, options: options}));
            modal.find('.single-selected-container').html(defaultSelectedItemTemplate({items: _currentSelected, uniID: elID, options: options}));

            if (!options.multiple) {
                if (_currentSelected.length > 0) {
                    modal.find('.single-item-selection').removeClass('hide');
                } else {
                    modal.find('.single-item-selection').addClass('hide');
                }
            }
        }

        /* UPDATE TABLE DATA FUNCTION 
         * @params selected = |array|object|null| fields required: { key }; overrides (global) selected
         */
        function updateSelected(usrSelected) {
            _selected = usrSelected;
        }

        /* ON SEARCH INPUT KEYUP 
         * @params keyword = |string|null| filter table data by keyword
         */
        function onSearchKeyup(keyword) {
            // console.log('onSearchKeyup', keyword);
        }
        
        function loader(status) {
            if (status) {
                modal.find('.browse-modal').removeClass('hide');
                modal.find('.modal-footer').find('[type="button"]').attr('disabled', true);
            } else {
                modal.find('.browse-modal').addClass('hide');
                modal.find('.modal-footer').find('[type="button"]').attr('disabled', false);
            }
        }

        function updateData(data, usrSelected) {
            // var usrSelected = (_.isUndefined(usrSelected) ? false : usrSelected);

            if (options.page.current == 0) {
                options.data = data;
            } else {
                options.data = options.data.concat(data);
            }

            updateTableData(options.data);
        }

        function getItemsTotal(field) {
            var total = 0;
            _.each(_selected, function(item) {
                if (typeof item.values[field] != undefined) {
                    total += parseFloat(item.values[field]);
                }
            });

            return total;
        }

        //-- CUSTOM SCRIPT HERE
        $(document).ready(function() {
            //-- intialize datepicker input
            modal.find(".datepicker").datetimepicker({
                format: 'MM/DD/YYYY',
            }).on('dp.change', function() {
                setFilter($(this).attr("name"), $(this).val());
            });

            //--- set filter on field input
            modal
                .find('.modal-filters input')
                    .on('input', function(e) {
                        setFilter($(this).attr("name"), $(this).val());
                    })
                    .on('keydown', function(e) {
                        if (e.which == 13) {
                            options.page.current = 0;
                            fetchData();
                        }
                    });
            
            //-- initialize select2 for the modal
            modal
                .find('.modal-filters select')
                    .select2({
                        dropdownParent: modal
                    })
                    .on("select2:select", function(e) {
                        setFilter(e.target.name, e.target.value);
                    });

            //--- initialize hidden input value
            $.each($('.modal-filters [type=hidden]'), function() {
                setFilter($(this).attr("name"), $(this).val());
            });

            //--- bind filter btn to fetchdata
            modal
                .find('.filter-btn')
                    .click(function() {
                        options.page.current = 0;
                        fetchData();
                    });

            modal
                .find('.modal-body-limi-height')
                    .scroll(function() {
                        var currentScroll = $(this).scrollTop() + $(this).height(),
                            maxScroll = $(this).find('.modal-body-content').height();

                            if (currentScroll == maxScroll && pageStatus.loaded != pageStatus.all) {
                                options.page.current++;
                                fetchData();
                            }
                    });

            $(document)
                    //--- add on checkbox on change
                .on("change", `.addon-checkbox-${elID}` , function(e) {
                    var checked = e.target.checked,
                        productID = $(this).attr("product"),
                        addonID = e.target.value,
                        type = $(this).attr("addon_type"),
                        quantity = $(this).attr("addon_quantity"),
                        value = $(this).attr("addon_value"),
                        price = $(this).attr("addon_price")
                        searchArea = [_currentSelected];
                    
                    if ($(this).hasClass("selected-checkbox")) {
                        searchArea = [_selected];
                    }

                    if (checked) {
                        _.each(searchArea, function(area) {
                            _.find(area, function(item) {
                                if (item.key == productID) {
                                    // console.log(item);
                                    var selected = {
                                        group: addonID,
                                        option: 0,
                                        price: price,
                                        quantity: quantity ? quantity : 1,
                                        value: value ? value : '',
                                        type: type,
                                        product: item.key
                                    };
                                    
                                    _.find(item.values.add_ons, function(addon) {
                                        if (addon.id == addonID) {
                                            if (type == 'option') {
                                                _.find(addon.items, function(option) {
                                                    if (option.is_default == 1) {
                                                        option.selected = {
                                                            group: addonID,
                                                            option: option.id,
                                                            price: option.added_price,
                                                            quantity: quantity ? quantity : 1,
                                                            value: value ? value : '',
                                                            type: type,
                                                            product: item.key
                                                        };

                                                        selected.selectedOptions = [option.selected];
                                                    }
                                                });
                                            }

                                            addon.selected = Object.assign({}, selected);
                                            return 1;
                                        }
                                    });
    
                                    item.selectedAddons.push(selected);
                                    return 1;
                                }
                            });
                        });
                    } else {
                        _.each(searchArea, function(area) {
                            _.find(area, function(item) {
                                if (item.key == productID) {
                                    
                                    _.find(item.values.add_ons, function(addon) {
                                        if (addon.id == addonID) {

                                            if (addon.items) {
                                                _.each(addon.items, function(option) {
                                                    delete option.selected;
                                                });
                                            }

                                            delete addon.selected;
                                            return 1;
                                        }
                                    });
    
                                    item.selectedAddons = _.filter(item.selectedAddons.slice(), function(sAddon) {
                                                                return sAddon.group != addonID;
                                                            });
                                    return 1;
                                }
                            });
                        });
                    }

                    if ($(this).hasClass("selected-checkbox")) {
                        processSelectedAddonsData(_selected);
                        displaySelectedData();
                        onConfirmCallBack(_selected);
                    } else {
                        updateCurrentlySelectedData();
                    }
                })
                //--- add on checkbox on change
                .on("change", `.addon-option-checkbox-${elID}` , function(e) {
                    var checked = e.target.checked,
                        productID = $(this).attr("product"),
                        group = $(this).attr("group-key"),
                        optionID = e.target.value,
                        type = $(this).attr("addon_type"),
                        quantity = $(this).attr("addon_quantity"),
                        value = $(this).attr("addon_value"),
                        price = $(this).attr("addon_price"),
                        searchArea = [_currentSelected];
                        
                    if ($(this).hasClass("selected-checkbox")) {
                        searchArea.push(_selected);
                    }
                        
                    if (checked) {
                        _.each(searchArea, function(area) {
                            _.find(area, function(item) {
                                if (item.key == productID) {
                                    // console.log(item);
                                    var selected = {
                                        group: group,
                                        option: optionID,
                                        price: price,
                                        quantity: quantity ? quantity : 1,
                                        value: value ? value : '',
                                        type: type,
                                        product: item.key
                                    };
                                    
                                    _.find(item.values.add_ons, function(addon, i) {
                                        if (addon.id == group) {
                                            
                                            if (addon.select_type == 'single' || addon.selected.selectedOptions == undefined) {
                                                addon.selected.selectedOptions = [];
                                            }
    
                                            addon.selected.selectedOptions.push(selected);
    
                                            _.each(addon.items, function(option) {
                                                if (option.id == optionID) {
                                                    option.selected = selected;
                                                } else {
                                                    if (addon.select_type == 'single') {
                                                        delete option.selected;
                                                    }
                                                }
                                            });
                                            
                                            item.selectedAddons = _.filter(item.selectedAddons.slice(), function(sAddon) {
                                                return sAddon.group != group;
                                            });

                                            item.selectedAddons.push(addon.selected);
    
                                            return true;
                                        }                                    
                                    });
                                    // console.log(item.values.add_ons);
                                    return 1;
                                }
                            });
                        });
                    } else {
                        _.each(searchArea, function(area) {
                            _.find(area, function(item) {
                                if (item.key == productID) {
                                    
                                    var addon = _.find(item.values.add_ons, function(addon) {
                                        if (addon.id == group) {
                                            addon.selected.selectedOptions = _.filter(addon.selected.selectedOptions, function(sOption) {
                                                                        if (sOption.id == optionID) {
                                                                            delete sOption.selected;
                                                                        } else {
                                                                            return 1;
                                                                        }
                                                                });

                                            if (addon.selected.selectedOptions.length == 0) {
                                                delete addon.selected;
                                            }
    
                                            return 1;
                                        }
                                    });
    
                                    if (addon.selected == undefined) {
                                        item.selectedAddons = _.filter(item.selectedAddons, function(sAddon) {
                                                                return sAddon.group != group;
                                                            });
                                    } else {
                                        _.find(item.selectedAddons, function(sAddon) {
                                            if (sAddon.group == group) {
                                                sAddon.selectedOptions = _.filter(sAddon.selectedOptions, function(sOption) {
                                                    return sOption.option != optionID;
                                                });
                                                console.log(sAddon);
                                                return 1;
                                            }
                                        });
                                    }
                                    
                                    return 1;
                                }
                            });
                        });
                    }

                    updateCurrentlySelectedData();

                    if ($(this).hasClass("selected-checkbox")) {
                        processSelectedAddonsData(_selected);
                        displaySelectedData();
                        onConfirmCallBack(_selected);
                    }
                })
                //--- on addon value change
                //--- this only applies to image and text type addons
                .on("input", `.addon-value-${elID}` , function(e) {
                    changeAddonValue($(this), e);
                })
                .on("change", `.addon-value-${elID}` , function(e) {
                    changeAddonValue($(this), e);
                });
                
                function changeAddonValue(el, e) {
                    var _this = el,
                        group = _this.attr("group"),
                        product = _this.attr("product"),
                        searchPool = _currentSelected,
                        imagePath = '';
                        
                    if (_this.hasClass("selected-value")) {
                        searchPool = _selected;
                    }

                    if (_this.hasClass('image-data')) {
                        var parentContainer = _this.parents(".image-data-container");
                        imagePath = parentContainer
                                        .find('img');
                    }
                                        
                    _.find(searchPool, function(item) {
                        if (item.key == product) {
                            if (item.selectedAddons) {
                                _.find(item.selectedAddons, function(sAddon) {
                                    if (sAddon.group == group) {
                                        sAddon.value = e.target.value;
    
                                        if (sAddon.value && imagePath.length > 0) {
                                            setTimeout(function() {
                                                sAddon.image = imagePath[0].getAttribute('src');
                                            }, 100)
                                        } else {
                                            sAddon.image = "";
                                        }
    
                                        _.find(item.values.add_ons, function(addon) {
                                            if (addon.id == group) {
                                                addon.selected.value = e.target.value;
                                                
                                                if (imagePath.length > 0) {
                                                    setTimeout(function() {
                                                        addon.selected.image = imagePath[0].getAttribute('src');
                                                    }, 100)
                                                } else {
                                                    addon.selected.image = "";
                                                }
                                            }
                                        });
    
                                        return 1;
                                    }
                                });
                            }
                        }
                    });
                    
                    if (_this.hasClass("selected-value")) {
                        $(`[name='addons_checkboxes[]'][value=${group}][product=${product}]`).attr("addon_value", e.target.value);
                        onConfirmCallBack(_selected);
                    }
                }
        });
		
        return {
            initializeData: function(data, callback) {
                var ids = [],
                    _button = $($(this)[0].target[0]);
                    validData = false;
                    
                if (Array.isArray(data)) {
                    if (data.length > 0) {
                        _.each(data, function(item) {
                            ids.push(item.id);
                        });
                        validData = true;
                    }
                } else if (typeof data == 'object') {
                    if (data.hasOwnProperty('id')) {
                        ids.push(data.id);
                        validData = true;
                    }
                }

                if (validData) {
                    _button.prop("disabled", true);

                    showLoaderOnSelectedContainer(true);
                    xhrRequest = $.ajax({
                        type: "GET",
                        url: options.apiURL,
                        data: {
                            ids: ids,
                            page: {size: 0}
                        },
                        dataType: "JSON",
                        success: function (result) {
                            var _parsedData = [];
                            if (result.data.length > 0) {
                                var selectedCount = 0;
                                _.map(result.data, function (val) {

                                    if (val.children.length > 0) {
                                        _.each(val.children, function(child) {
                                            child.html = selectorItemVariantTemplate(child.values);
                                        });
                                    }
                                    val.quantifiable = options.itemsQuantifiable;
                                    _parsedData.push({
                                        children: val.children,
                                        values: _.omit(val, 'variants', 'children'),
                                        key: val.id,
                                        timestamp: moment().unix() + selectedCount,
                                        html: selectorItemTemplate(val),
                                    });

                                    selectedCount++;
                                });

                                _currentSelected = _parsedData.slice();
                                _selected = _parsedData.slice();
                                displaySelectedData();
                                _button.prop("disabled", false);
                            } 

                            if (typeof callback == 'function') {
                                onConfirmCallBack = callback;
                                callback(_selected);
                            }

                            loader(false);
                        },
                        error: function (xhr) {
                            console.log(xhr);
                        },
                    });
                }
            },
            initializeOrderData: function(data, callback) {
                if (!options.orderSelector) {
                    return false;
                }

                var ids = [],
                    _button = $($(this)[0].target[0]);
                    validData = false;
                
                if (Array.isArray(data)) {
                    if (data.length > 0) {
                        _.each(data, function(item) {
                            ids.push(item.reference_id);
                        });
                        validData = true;
                    }
                } else if (typeof data == 'object') {
                    if (data.hasOwnProperty('id')) {
                        ids.push(data.reference_id);
                        validData = true;
                    }
                }

                if (validData) {
                    _button.prop("disabled", true);

                    showLoaderOnSelectedContainer(true);
                    xhrRequest = $.ajax({
                        type: "GET",
                        url: options.apiURL,
                        data: {
                            ids: ids,
                            page: {size: 0}
                        },
                        dataType: "JSON",
                        success: function (result) {
                            var _parsedData = [];
                            if (result.data.length > 0) {
                                var selectedCount = 0;
                                _.map(result.data, function (val) {
                                    var orderItem = _.find(data, function(item) {
                                                        return item.reference_id == val.id;
                                                    });

                                    if (val.children.length > 0) {
                                        _.each(val.children, function(child) {
                                            child.html = selectorItemVariantTemplate(child.values);
                                        });
                                    }

                                    val.quantifiable = options.itemsQuantifiable;

                                    if (orderItem) {
                                        val.selectedAddons = [];
                                        val.quantity = orderItem.quantity;
                                        
                                        if (orderItem.add_ons != undefined) {
                                            if (orderItem.add_ons.length > 0) {
                                                _.each(orderItem.add_ons, function(oAddon) {
                                                    _.find(val.add_ons, function(addon) {
                                                        if (addon.id == oAddon.addon_group && addon.type == oAddon.type) {
                                                            if (addon.selected == undefined) {
                                                                addon.selected = {
                                                                    group: addon.id,
                                                                    option: 0,
                                                                    price: addon.price,
                                                                    quantity: oAddon.qty,
                                                                    type: addon.type,
                                                                    value: oAddon.value,
                                                                    product: val.id
                                                                };
                                                            }

                                                            if (oAddon.addon_option != 0) {
                                                                if (addon.selected.selectedOptions == undefined) {
                                                                    addon.selected.selectedOptions = [];
                                                                }

                                                                _.find(addon.items, function(option) {
                                                                    if (option.id == oAddon.addon_option) {
                                                                        option.selected = {
                                                                            group: addon.id,
                                                                            option: option.id,
                                                                            price: option.added_price,
                                                                            quantity: oAddon.qty,
                                                                            type: addon.type,
                                                                            value: oAddon.value,
                                                                            product: val.id
                                                                        }

                                                                        addon.selected.selectedOptions.push(option.selected);
                                                                        return 1;
                                                                    }
                                                                });
                                                            }

                                                            if (oAddon.type == 'image') {
                                                                addon.selected.image = oAddon.image_value ? assetURL + oAddon.image_value.path : '';
                                                            }
                                                            
                                                            if(!_.find(val.selectedAddons, function(sAddon) {
                                                                if (sAddon.group == addon.selected.group) {
                                                                    sAddon = addon.selected;
                                                                    return 1;
                                                                }
                                                            })) {
                                                                val.selectedAddons.push(addon.selected);
                                                            }
                                                            console.log(val.selectedAddons);
                                                            return 1;
                                                        }
                                                    });
                                                });
                                            }
                                        }
                                    }
                                    
                                    _parsedData.push({
                                        children: val.children,
                                        selectedAddons: val.selectedAddons.slice(),
                                        values: _.omit(val, 'variants', 'children', 'selectedAddons'),
                                        key: val.id,
                                        timestamp: moment().unix() + selectedCount,
                                        html: selectorItemTemplate(val),
                                    });

                                    selectedCount++;
                                });

                                _selected = _parsedData.slice();

                                processSelectedAddonsData(_selected);
                                displaySelectedData();
                                _button.prop("disabled", false);
                            } 

                            if (typeof callback == 'function') {
                                onConfirmCallBack = callback;
                                callback(_selected);
                            }

                            loader(false);
                        },
                        error: function (xhr) {
                            console.log(xhr);
                        },
                    });
                }
            },
            /* SHOW BROWSE MODAL 
             * @params keyword = |string|null| default search keyword value on show
             * @params selected = |array|object|null| fields required: { key }; overrides options.data
             */
            show: function (keyword, usrSelected) {
                showModal(true);
            },
            /* HIDE BROWSE MODAL */
            hide: function() {
                showModal(false);
            },
            /* SHOW/HIDE LOADER 
             * @params status = |bool|required| action parameter to show/hide loader 
             */
            loader: function(status) {
                loader(status);
            },
            /* UPDATE MODAL DATA (will update table data) 
             * @params data = |array|required| fields required: { values, key, text, html, children }; ovverides options.data
             * @params selected = |array|object|null| fields required: { key }; overrides options.data
             */
            updateData: function(data, usrSelected) {
                updateData(data, usrSelected);
            },
            /* UPDATE OPTIONS (will update options data) 
             * @params newOptions = |object|null| extends to options
             */
            updateOptions: function(newOptions) {
                // Extend those new options
                options = $.extend(options, newOptions);
            },
            /* ON CONFIRM BUTTON CLICK CALLBACK 
             * @params callback = |func|required| called when keyup event is triggered from jQuery
             */
            onConfirm: function(callback) {
                modal.find('.modal-confirm-button').on('click', function (e) {
                    var addedItems = [];
                    // console.log(_currentSelected);
                    // pick out all the newly added items
                    _.each(_currentSelected, function(current) {
                        if (!_.find(_selected, function(selected) {
                            return selected.key == current.key;
                        })) {
                            addedItems.push(current);
                        }
                    });

                    _selected = _currentSelected;
                    displaySelectedData();

                    /* callback function
                     * @params _selected = overall selected including previous selected
                     * @params addedItems = newly added items
                     */
                    if (typeof callback == 'function') {
                        onConfirmCallBack = callback;
                        callback(_selected, addedItems);
                    }
                });
            },
            onShow: function(callback) {
                fetchData();
                modal.on('shown.bs.modal', callback);
            },
            beforeShow: function(callback) {
                modal.on('show.bs.modal', callback);
            },
            getItemsTotal: function(field) {
                return getItemsTotal(field);
            },
            getDisplayTotals: function() {
                return processDataTotals(true);
            },
            modal: modal,
            target: $(this)
        };

        // HELPER FUNCTIONS
        function processSelectedAddonsData (data) {

            if(data == undefined) {
                data = _currentSelected;
            }

            _.each(data, function(item) {
                var totalPrice = parseFloat(item.values.price);

                if (item.selectedAddons == undefined) {
                    item.selectedAddons = [];
                }
                
                _.each(item.values.add_ons, function(addon) {
                    if (addon.is_required) {
                        if (addon.selected == undefined) {

                            addon.selected = {
                                group: addon.id,
                                option: 0,
                                quantity: 1,
                                price: addon.price,
                                value: "",
                                type: addon.type,
                                product: item.key
                            }

                            if (addon.type == 'option') {
                                _.each(addon.items, function(aItem) {
                                    if (aItem.is_default) {
                                        aItem.selected = {
                                            group: aItem.group,
                                            option: aItem.id,
                                            quantity: 1,
                                            value: "",
                                            price: aItem.added_price,
                                            type: addon.type,
                                            product: item.key
                                        }
                                        
                                        addon.selected.selectedOptions = [];
                                        addon.selected.selectedOptions.push(Object.assign({}, aItem.selected));
                                        item.selectedAddons.push(Object.assign({}, addon.selected));
                                    }
                                });
                            } else {
                                item.selectedAddons.push(Object.assign({}, addon.selected));
                            }
                        }
                    } else {
                        if (addon.selected) {
                            if (addon.type != 'option') {
                                var addOnSearch = _.find(item.selectedAddons, function(sAddon) {
                                    return sAddon.group == addon.id && sAddon.option == 0;
                                });
    
                                if (!addOnSearch) {
                                    delete addon.selected;
                                }

                            } else {
                                _.each(addon.items, function(aItem) {
                                    var addOnSearch = _.find(item.selectedAddons, function(sAddon) {
                                            return sAddon.group == aItem.group
                                    });

                                    if(addOnSearch) {
                                        addOnSearch = _.find(addOnSearch.selectedOptions, function(sOption) {
                                            return sOption.option == aItem.id;
                                        });
                                    }

                                    if (!addOnSearch) {
                                        delete aItem.selected;
                                    }
                                });
                            }
                        }
                    }
                });
                
                _.each(item.selectedAddons, function(sAddon) {
                    if (sAddon.type != "option") {
                        totalPrice += parseFloat(sAddon.price) * parseFloat(sAddon.quantity);
                    } else {
                        _.each(sAddon.selectedOptions, function(sOption) {
                            totalPrice += parseFloat(sOption.price) * parseFloat(sOption.quantity);
                        });
                    }
                });

                item.values.totalPrice = parseFloat(totalPrice).toFixed(2);
            });
        }

        function filterDataTotals() {
            var filteredTotals = [];

            if (!Array.isArray(options.displayTotal)) {
                data = [options.displayTotal];
            } else {
                data = options.displayTotal;
            }

            _.each(data, function(displayTotal) {
                if (typeof displayTotal == 'object') {
                    if (displayTotal.hasOwnProperty('object') && displayTotal.object) {
                        
                        if (!displayTotal.hasOwnProperty('label')) {
                            displayTotal.label = displayTotal.object;
                        }
                        
                        filteredTotals.push(displayTotal);
                    }
                }
            });

            return filteredTotals;
        }

        /**
         * 
         * @param selected {*boolean value if the search will be in the _selected data} 
         */
        function processDataTotals(selected) {
            var processedTotals = [];
            var searchPool = _currentSelected;

            if (selected) {
                searchPool = _selected;
            }

            if (!Array.isArray(options.displayTotal)) {
                data = [options.displayTotal];
            } else {
                data = options.displayTotal;
            }

            _.each(data, function(totalData) {
                if (typeof totalData == 'object') {
                    if (totalData.hasOwnProperty('object')) {
                        var totalObj = {
                                label: totalData.label ? totalData.label : totalData.object,
                                dataAttr: totalData.object,
                                total: 0
                            },
                            totalOptions = totalData.options;

                        _.each(searchPool, function(item) {
                            if (item.values[totalObj.dataAttr]) {
                                var tempTotal = parseFloat(item.values[totalObj.dataAttr]);

                                if (totalOptions.combineWith && totalOptions.combineOperation) {
                                    if (item.values[totalOptions.combineWith]) {
                                        tempTotal = eval(`parseFloat(${tempTotal}) ${totalOptions.combineOperation} parseFloat(${item.values[totalOptions.combineWith]})`);
                                    }
                                }

                                totalObj.total += tempTotal;
                            }
                        });

                        if (totalOptions.decimal) {
                            totalObj.total = numberWithCommas(parseFloat(totalObj.total).toFixed(totalOptions.decimal));
                        }

                        processedTotals.push(totalObj);
                    }
                }
            });

            return processedTotals;
        }

        /**
         * 
         * @param totals {*objects of totals to be displayed in the selector}
         */
        function displayDataTotals(totals) {
            _.each(totals, function(totalData) {
                modal.find(`[total-key=${totalData.dataAttr}] span`).html(totalData.total);
            });
        }

        function displaySelectedData() {
            if (options.selectedDataContainer) {
                if (_selected.length > 0) {
                    // console.log(_selected);
                    $(options.selectedDataContainer).html(defaultSelectedItemTemplate({items: _selected, uniID: elID, options: options, selectedContainer: true}));

                    if (options.orderSelector) {
                        _.each(_selected, function(item) {
                            item.animationDuration = '0s';
                        });
                    }
                } else {
                    $(options.selectedDataContainer).html(`
                        <div class="empty-message">
                            ${options.emptyMessage ? options.emptyMessage : 'No items selected'}
                        </div>
                    `);
                }
            }
        }

        function showLoaderOnSelectedContainer(loading) {
            if (options.selectedDataContainer) {
                if (loading) {
                    $(options.selectedDataContainer).html(`
                        <div class="elipsis-loader">
                            <div></div>
                            <div style="animation-delay: 0.5s"></div>
                            <div style="animation-delay: 1s"></div>
                        </div>
                    `);
                }
            }
        }

        function initializeFilters(filters) {
            var filterObjs = {};
            
            _.each(filters, function(filter) {
                if (filter.name) {
                    filterObjs[filter.name] = {
                        "value": ""
                    };

                    if (filter.query != undefined) {
                        var query = {};
                        if (filter.query.hasOwnProperty('method')) {
                            query["method"] = filter.query.method;
                        }

                        if (filter.query.hasOwnProperty('logic')) {
                            query["logic"] = filter.query.logic;
                        }

                        if (filter.query.hasOwnProperty('wildcard')) {
                            query["wildcard"] = filter.query.wildcard;
                        }

                        if (filter.query.hasOwnProperty('relationship')) {
                            query["relationship"] = filter.query.relationship;
                        }

                        if (query.hasOwnProperty('method') && query.hasOwnProperty('logic')) {
                            filterObjs[filter.name]['query'] = query;
                        }
                    }
                }
            });

            return filterObjs;
        }

        function setFilter(name, value) {
            if (requestFilters[name] != undefined) {
                requestFilters[name]['value'] = value;
            }
        }

        function clearFilters() {
            _.each(requestFilters, function(filter, key) {
                var formEl = modal.find(`[name="${key}"]`);

                if (formEl.attr("type") != "hidden") {
                    formEl.val('');
                    filter['value'] = '';
                }
            });
        }
    }

    /* CUSTOMIZED HANDLE BARS HELPERS */
    /* HANDLEBARS HELPER: LENGTH CONDITION RETURNS boolean
    * @param data |array|required array to check length
    */
    Handlebars.registerHelper('length', function (arr, options) {
        return (arr.length > 0) ? options.fn(this) : options.inverse(this);
    });
    Handlebars.registerHelper('elseLength', function (arr, options) {
        return (arr.length == 0) ? options.fn(this) : options.inverse(this);
    });

    /* HANDLEBARS HELPER: HIDE CLASS FROM BOOLEAN 
    * @param bool |boolean|required boolean to parse
    */
    Handlebars.registerHelper('hide', function (bool) {
       return (bool) ? 'hide' : '';
    });

    /* HANDLEBARS HELPER: SET PROP CHECKED FROM BOOLEAN 
    * @param bool |booleanarray|required boolean to parse
    */
    Handlebars.registerHelper('check', function (bool) {
        return (bool) ? 'checked' : '';
    });
    /* CUSTOMIZED HANDLE BARS HELPERS -- END */

    Handlebars.registerHelper('numberwithcommas', function (number) {
        return numberWithCommas(number);
    });

    function sumoGUID() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };
})(jQuery);