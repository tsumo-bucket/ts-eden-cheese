@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminVideoOptions')}}">Video Options</a></li>
  <li class="active">View</li>
</ol>
@stop

@section('content')
<div class="col-md-8">
	<table class='table table-striped table-bordered table-view'>
		<tr>
			<th>Id</th>
			<td>{!!$data->id!!}</td>
		</tr>
		<tr>
			<th>Autoplay</th>
			<td>{!!$data->autoplay!!}</td>
		</tr>
		<tr>
			<th>Loop</th>
			<td>{!!$data->loop!!}</td>
		</tr>
		<tr>
			<th>Controls</th>
			<td>{!!$data->controls!!}</td>
		</tr>
		<tr>
			<th>Mute</th>
			<td>{!!$data->mute!!}</td>
		</tr>
		<tr>
			<th>Fullscreen</th>
			<td>{!!$data->fullscreen!!}</td>
		</tr>
		<tr>
			<th>Frameborder</th>
			<td>{!!$data->frameborder!!}</td>
		</tr>
		<tr>
			<th>Related videos</th>
			<td>{!!$data->related_videos!!}</td>
		</tr>
		<tr>
			<th>Gyroscope</th>
			<td>{!!$data->gyroscope!!}</td>
		</tr>
		<tr>
			<th>Accelerometer</th>
			<td>{!!$data->accelerometer!!}</td>
		</tr>
		<tr>
			<th>Picture</th>
			<td>{!!$data->picture!!}</td>
		</tr>
		<tr>
			<th>Encrypt media</th>
			<td>{!!$data->encrypt_media!!}</td>
		</tr>
		<tr>
			<th>Start</th>
			<td>{!!$data->start!!}</td>
		</tr>
		<tr>
			<th>Portrait</th>
			<td>{!!$data->portrait!!}</td>
		</tr>
		<tr>
			<th>Title</th>
			<td>{!!$data->title!!}</td>
		</tr>
		<tr>
			<th>Byline</th>
			<td>{!!$data->byline!!}</td>
		</tr>
		<tr>
			<th>Created at</th>
			<td>
				@if ($data->created_at)
				<?php $created_at = new Carbon($data->created_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
		<tr>
			<th>Updated at</th>
			<td>
				@if ($data->updated_at)
				<?php $created_at = new Carbon($data->updated_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
	</table>
</div>
@stop