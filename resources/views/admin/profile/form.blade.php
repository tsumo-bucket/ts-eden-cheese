<div class="caboodle-form-group">
 <label for="name">Name</label>
 {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
 <label for="email">Email</label>
 {!! Form::email('email', null, ['class'=>'form-control', 'id'=>'email', 'placeholder'=>'Email', 'required']) !!}
</div>