@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item far">
			<a href="{{route('adminDashboard')}}">Dashboard</a>
		</li>
		<li class="breadcrumb-item far">
			<span>Dev Modules</span>
		</li>
		<li class="breadcrumb-item far">
			<a href="{{route('adminPages')}}">Pages</a>
		</li>
		<li class="breadcrumb-item far">
			<a href="{{ route('adminPagesEdit', $page->id) }}">{{ $page->name }}</a>
		</li>
		<li class="breadcrumb-item far">
			<a href="{{ route('adminPageContentsEdit', $content->id) }}">Content</a>
		</li>
		<li class="breadcrumb-item far active">
			<span>Edit Item</span>
		</li>
	</ol>
</nav>
@stop

@section('header')
<header class="flex-center">
	<h1>{{ $title }}</h1>
	<div class="header-actions">
		<a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{ route('adminPageContentsEdit', $content->id) }}">Cancel</a>
		<button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save Changes</button>
	</div>
</header>
@stop

@section('footer')
<footer>
	<div class="text-right">
		<a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{ route('adminPageContentsEdit', $content->id) }}">Cancel</a>
		<button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save Changes</button>
	</div>
</footer>
@stop

@section('content')
{!! Form::model($data, ['route'=>['adminPageContentItemsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit form-clear']) !!}
	@include('admin.dev.page_content_items.form')
{!! Form::close() !!}
@stop

@section('added-scripts')
	@include('admin.dev.page_controls.scripts')
@stop